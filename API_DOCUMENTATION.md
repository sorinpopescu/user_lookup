# User lookup

The API will return HTTP code `200` for all successful requests. For unsuccessful requests a relavent HTTP code will be returned.

For any request with missing parameters the API will return HTTP code `400` with the response:

	{
    code: 400,
    message: "Query parameter "term" is empty",
    errors: null
    }

# Resources 

# 1. Get User

Accepts a string as term parameter and dupes as an bool and returns an order name list.

## Resource method name

	
    Get /user
	

## Request parameters

Parameter | Type     | Required | Description 
--------- | -------- | -------- | -----------
term     | _string_ | yes      | Term to search for
dupes  | _bool_ | no      | Eliminate duplicate names