
User Lookup
=============

This README describes how to install and maintain User Lookup application.

For using the user lookup API, see the [API documentation](API_DOCUMENTATION.md).

Installation (prerequisites)
----------------------------

This guide is based on using Debian Wheezy 64. A fresh OS install
is assumed, but it can be adapted for adding to an existing install.

Your user must be in the www-data group for the permissions steps to work.

Install required software:

    sudo apt-get install acl libapache2-mod-php5 apache2 php5 php5-sqlite \
        php-pear libxml2-dev php5-intl php5-mysql libpcre3-dev php5-curl \
        unzip git php5-xsl php5-dev

Enable mod rewrite:

    sudo a2enmod rewrite

Configuration (prerequisites)
-----------------------------

To configure PHP, edit both of the following files in turn:

    /etc/php5/apache2/php.ini
    /etc/php5/cli/php.ini

For each file, change these values:

    date.timezone = Europe/London
    short_open_tag = Off

Installation (project)
----------------------

Clone the application from git into /var/www:

    cd /var/www
    sudo chgrp www-data /var/www
    sudo chmod 0775 /var/www
    git clone git clone git@bitbucket.org:sorinpopescu/user_lookup.git

Set up a vhost as follows, file /etc/apache2/sites-available/user_lookup.conf (change ServerName as needed):

    <VirtualHost *:80>
        ServerName user-lookup.local
        DocumentRoot /var/www/user_lookup/web

        <Directory /var/www/user_lookup/>
            Options Indexes FollowSymLinks MultiViews
            AllowOverride All
            Order allow,deny
            Allow from all
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/user-lookup-error.log
        LogLevel warn
        CustomLog ${APACHE_LOG_DIR}/user-lookup-access.log combined
    </VirtualHost>

Restart Apache:

    sudo a2ensite user_lookup.conf
    sudo apache2ctl configtest
    sudo service apache2 restart

Composer Update:

    php composer.phar install

    When creating parameter.yml you must provide the absolute path to the name list file!

Setting permission:

    HTTPDUSER=`ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
    sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var
    sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var

Tests
-----

You can run the tests using phpunit:

    cd /var/www/user_lookup
    phpunit


Coding Standards
----------------

This project follows the PSR-2 coding standards:

    https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md
