<?php

namespace AppBundle\Controller;

use AppBundle\Exception\BaseException;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use AppBundle\Service\UserService;

class DefaultController extends FOSRestController
{
    private $userService;

    public function init()
    {
        $this->userService = $this->get('user_service');
    }

    /**
     * @Get("/user")
     * @QueryParam(name="term", requirements="\D+", strict=true, nullable=false, description="Event Name")
     * @QueryParam(name="dupes", requirements="(1|0)", strict=true, nullable=true, description="Event Data")

     * @param ParamFetcher $paramFetcher

     * @return array
     */
    public function userAction(ParamFetcher $paramFetcher)
    {
        $this->init();

        /* extracting parameters from ParameterFetcher */
        $term = $paramFetcher->get('term');
        $dupes = (bool) $paramFetcher->get('dupes');
        
        $users = $this->userService->getUsers($term, $dupes);

        return $users;
    }
}
