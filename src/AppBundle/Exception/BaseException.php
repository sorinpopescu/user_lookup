<?php

namespace AppBundle\Exception;

/**
 * Class BaseException
 * @package AppBundle\Exception
 */
class BaseException extends \Exception
{
}