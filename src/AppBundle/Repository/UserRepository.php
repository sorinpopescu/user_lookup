<?php

namespace AppBundle\Repository;


use Symfony\Component\HttpFoundation\File\Exception\FileException;

/**
 * Class UserRepository
 * @package AppBundle\Repository
 */
class UserRepository implements UserRepositoryInterface
{
    /**
     * @var string
     */
    protected $filePath;

    /**
     * UserRepository constructor.
     * @param $filePath
     */
    public function __construct($filePath)
    {
        /* absolute path to the file is injected in the repository from parameters */
        $this->filePath = $filePath;
    }

    /**
     * @return array
     */
    public function getUsers()
    {
        /* i'm getting all the users from the file and not just the ones that match the term so we can have flexibility
        for the future and be able to reuse this method for different scenarios */
        $users = [];
        $handle = fopen($this->filePath, "r");
        if (!$handle) {
            throw new FileException('Invalid file!');
        }

        while (($line = fgets($handle)) !== false) {
            $result = preg_split('/\s+/', $line);
            $users[] = [
                'first_name' => $result[0],
                'last_name' => $result[1]
            ];
        }

        fclose($handle);

        return $users;
    }
}