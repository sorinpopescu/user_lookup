<?php

namespace AppBundle\Repository;


/**
 * Interface UserRepositoryInterface
 * @package AppBundle\Repository
 */
interface UserRepositoryInterface
{
    public function getUsers();
}