<?php

namespace AppBundle\Service;


use AppBundle\Repository\UserRepositoryInterface as UserRepository;

class UserService
{
    private $repository;

    public function __construct(UserRepository $repository)
    {
        /* Repository interface is injected in the service. An interface is being used so we are flexible with future
        changes in the way the users are extracted (maybe in the future we'll have a user database not a file) */
        $this->repository = $repository;
    }

    /**
     * @param $term
     * @param $dupes
     * @return array
     */
    public function getUsers($term, $dupes)
    {
        /* call to repository getUsers to retrieve the list of names from file */
        $users = $this->repository->getUsers();

        /* we remove from the list names that don't match the given term */
        list($fullName, $users) = $this->match($term, $users);

        /* if dupes is set then we return the list of names without duplicates */
        if ($dupes) {
            return $this->getUniqueUsers($fullName, $users);
        }

        /* if dupes is set then we return the list of names with duplicates */
        return $users;
    }

    /**
     * @param $term
     * @param $users
     * @return array
     */
    public function match($term, $users)
    {
        $fullName = [];
        foreach ($users as $key => $user) {
            /* if the name doesn't match the given term we remove it from the list and continue to the next one */
            if (!$this->isMatch($user, $term)) {
                unset($users[$key]);
                continue;
            }
            /* if the name match the term we create a new array of concatenate names with the key of the original
            name in order to be able to sort and remove duplicate names */
            $fullName[$key] = $user['last_name'] . $user['first_name'];
        }

        /* sort the name array by last and first name */
        array_multisort($fullName, SORT_ASC, $users);

        return array($fullName, $users);
    }

    /**
     * @param $user
     * @param $term
     * @return bool
     */
    public function isMatch($user, $term)
    {
        /* check if the term is contained in the name */
        if (strpos($user['first_name'].' '.$user['last_name'], $term) !== false) {
            return true;
        }

        return false;
    }

    /**
     * @param $fullName
     * @param $users
     * @return array
     */
    public function getUniqueUsers($fullName, $users)
    {
        $unique = [];
        /* in order to easily remove duplicates we're flipping creating an array with the keys as the last name
        and first name concatenated and the key of the name list as value. This will remove all duplicate keys from the
        array implicitly removing all duplicated names so all we have to do is to assign to a new array the users with
        the remaining keys */
        $fullName = array_flip($fullName);
        foreach ($fullName as $key) {
            $unique[] = $users[$key];
        }
        return $unique;
    }
}
