<?php

namespace Tests\AppBundle\Functional;


use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserServiceTest extends KernelTestCase
{
    private $container;

    protected function setUp()
    {
        self::bootKernel();

        $this->container = self::$kernel->getContainer();
    }

    public function testGetUniqueUsers()
    {
        $term = "Abraham";
        $dupes = true;
        $userService = $this->container->get('user_service');

        $result = $userService->getUsers($term, $dupes);

        $this->assertCount(73, $result);
    }

    public function testGetUsers()
    {
        $term = "Abraham";
        $dupes = false;
        $userService = $this->container->get('user_service');

        $result = $userService->getUsers($term, $dupes);

        $this->assertCount(86, $result);
    }

}
