<?php

namespace Tests\AppBundle\Unit;


use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserServiceRepositoryTest extends KernelTestCase
{
    private $container;

    protected function setUp()
    {
        self::bootKernel();

        $this->container = self::$kernel->getContainer();
    }

    public function testGetUsers()
    {
        $userRepository = $this->container->get('user_repository');

        $result = $userRepository->getUsers();

        $this->assertCount(12000, $result);
    }

}
