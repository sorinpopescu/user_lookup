<?php

namespace Tests\AppBundle\Unit;


use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserServiceTest extends KernelTestCase
{
    private $container;

    protected function setUp()
    {
        self::bootKernel();

        $this->container = self::$kernel->getContainer();
    }

    public function testIsMatch()
    {
        $user = [
            "first_name" => "Adam",
            "last_name" => "Abraham"
        ];

        $term = "dam";
        $userService = $this->container->get('user_service');

        $result = $userService->isMatch($user, $term);

        $this->assertTrue($result);
    }

    public function testIsNotMatch()
    {
        $user = [
            "first_name" => "Adam",
            "last_name" => "Abraham"
        ];

        $term = "damz";
        $userService = $this->container->get('user_service');

        $result = $userService->isMatch($user, $term);

        $this->assertFalse($result);
    }

    public function testMatch()
    {
        $users = [
            0 => [
                "first_name" => "Adam",
                "last_name" => "Abraham"
            ],
            1 => [
                "first_name" => "Alan",
                "last_name" => "Abraham"
            ],
            2 => [
                "first_name" => "Kevin",
                "last_name" => "Abraham"
            ],
            3 => [
                "first_name" => "Brandon",
                "last_name" => "Abraham"
            ]
        ];

        $term = "an";
        $userService = $this->container->get('user_service');

        list($fullName, $result) = $userService->match($term, $users);
        $this->assertEquals(
            [
                0 => [
                    "first_name" => "Alan",
                    "last_name" => "Abraham"
                ],
                1 => [
                    "first_name" => "Brandon",
                    "last_name" => "Abraham"
                ]
            ],
            $result
        );

        $this->assertEquals(
            [
                0 => "AbrahamAlan",
                1 => "AbrahamBrandon"
            ],
            $fullName
        );
    }

    public function testGetUniqueUsers()
    {
        $users = [
            0 => [
                "first_name" => "Adam",
                "last_name" => "Abraham"
            ],
            1 => [
                "first_name" => "Adam",
                "last_name" => "Abraham"
            ],
            2 => [
                "first_name" => "Kevin",
                "last_name" => "Abraham"
            ],
            3 => [
                "first_name" => "Brandon",
                "last_name" => "Abraham"
            ]
        ];

        $fullName = [
            0 => "AbrahamAdam",
            1 => "AbrahamAdam",
            2 => "AbrahamKevin",
            3 => "AbrahamBrandon"
        ];
        $userService = $this->container->get('user_service');

        $result = $userService->getUniqueUsers($fullName, $users);

        $this->assertEquals([
            0 => [
                "first_name" => "Adam",
                "last_name" => "Abraham"
            ],
            1 => [
                "first_name" => "Kevin",
                "last_name" => "Abraham"
            ],
            2 => [
                "first_name" => "Brandon",
                "last_name" => "Abraham"
            ]
        ], $result);
    }
}
